#!/usr/bin/env sh

if [ $UID -ne 0 ]; then
  sudo $0
  exit
fi

echo "Recommended drive config:"
echo "Boot,root,home: 1G SSD, writethrough, backup"
echo "Packages: 10G NVME, writethrough, NO backup (scale up for larger services)"
echo "Log: 2G HDD, unsafe, backup"
echo "Swap (optional): 8G SSD, unsafe, NO backup"
echo "Bulk (optional): XG HDD, no cache, backup"

echo
echo "Available drives:"
echo

lsblk

echo
echo "Input target drives"
read -p "Boot,root,home: " DRV_BOOTROOT
read -p "Packages: " DRV_PACKAGES
read -p "Log: " DRV_LOG
read -p "Swap (optional): " DRV_SWAP
read -p "Bulk (optional): " DRV_BULK

DRV_BOOTROOT="/dev/$DRV_BOOTROOT"
DRV_PACKAGES="/dev/$DRV_PACKAGES"
DRV_LOG="/dev/$DRV_LOG"
[ "$DRV_SWAP" != "" ] && DRV_SWAP="/dev/$DRV_SWAP"
[ "$DRV_BULK" != "" ] && DRV_BULK="/dev/$DRV_BULK"

echo "Prepating target drives:"
echo " - Boot,root,home: $DRV_BOOTROOT"
echo " - Packages: $DRV_PACKAGES"
echo " - Log: $DRV_LOG"
if [ "$DRV_SWAP" != "" ]
then
  echo " - Swap: $DRV_SWAP"
else
  echo " - Swap: (skipped)"
fi
if [ "$DRV_BULK" != "" ]
then
  echo " - Bulk: $DRV_BULK"
else
  echo " - Bulk: (skipped)"
fi
echo
read -p "Confirm? [y]" CONFIRM

if [ "$CONFIRM" != "y" ] && [ "$CONFIRM" != "Y" ] && [ "$CONFIRM" != "" ]; then
  exit
fi

INSTALL_MOUNT="/mnt"

parted $DRV_BOOTROOT -- mklabel gpt
parted $DRV_BOOTROOT -- mkpart primary 512MiB 100%
parted $DRV_BOOTROOT -- mkpart ESP fat32 1MiB 512MiB
parted $DRV_BOOTROOT -- set 2 esp on

mkfs.xfs -fL root "$DRV_BOOTROOT"1
mkfs.fat -F 32 -n boot "$DRV_BOOTROOT"2

mkfs.xfs -fL packages $DRV_PACKAGES
mkfs.xfs -fL log $DRV_LOG
[ "$DRV_SWAP" != "" ] && mkswap -L swap $DRV_SWAP
[ "$DRV_BULK" != "" ] && mkfs.xfs -fL bulk $DRV_BULK

mount /dev/disk/by-label/root $INSTALL_MOUNT

mkdir -p $INSTALL_MOUNT/boot
mount /dev/disk/by-label/boot $INSTALL_MOUNT/boot

mkdir -p $INSTALL_MOUNT/packages
mount /dev/disk/by-label/packages $INSTALL_MOUNT/packages

head -c 500M </dev/urandom >$INSTALL_MOUNT/packages/extra.space
mkdir -p \
  $INSTALL_MOUNT/packages/nix $INSTALL_MOUNT/nix \
  $INSTALL_MOUNT/packages/flatpak $INSTALL_MOUNT/var/lib/flatpak \
  $INSTALL_MOUNT/packages/oci $INSTALL_MOUNT/var/lib/containers \
  $INSTALL_MOUNT/packages/waydroid $INSTALL_MOUNT/var/lib/waydroid \
  $INSTALL_MOUNT/packages/unity $INSTALL_MOUNT/var/data/UnityHub
mount --bind $INSTALL_MOUNT/packages/nix $INSTALL_MOUNT/nix
mount --bind $INSTALL_MOUNT/packages/flatpak $INSTALL_MOUNT/var/lib/flatpak
mount --bind $INSTALL_MOUNT/packages/oci $INSTALL_MOUNT/var/lib/containers
mount --bind $INSTALL_MOUNT/packages/waydroid $INSTALL_MOUNT/var/lib/waydroid
mount --bind $INSTALL_MOUNT/packages/unity $INSTALL_MOUNT/var/data/UnityHub

mkdir -p $INSTALL_MOUNT/var/log
mount /dev/disk/by-label/log $INSTALL_MOUNT/var/log

if [ "$DRV_SWAP" != "" ]
then
  swapon /dev/disk/by-label/swap
fi

if [ "$DRV_BULK" != "" ]
then
  mkdir -p $INSTALL_MOUNT/bulk
  mount /dev/disk/by-label/bulk $INSTALL_MOUNT/bulk
fi

nix-channel --update
mkdir -p $INSTALL_MOUNT/etc/nixos/src
ln -s $INSTALL_MOUNT/etc/nixos/src /etc/nixos/src
