#! /usr/bin/env sh

user=$1

if [ -z "$user" ]; then
  read -p "Main user name: " user
fi

nixos-install --no-root-passwd && nixos-enter -c "passwd $user" && reboot
