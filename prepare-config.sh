#!/usr/bin/env nix-shell
#!nix-shell -i bash -p curl -p git

if [ $UID -ne 0 ]; then
  sudo $0
  exit
fi

installMount="/mnt"

nixosPath="$installMount/etc/nixos"
srcPath="$nixosPath/src"
[ -d "$srcPath" ] || mkdir -p "$srcPath"

echo
read -p "Clone default config? [y]" confirm

if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ] && [ "$confirm" != "" ]; then
  echo
  echo "Remember to set fileSystems.\"/packages\".neededForBoot = true; in your config or you will not be able to boot."
else
  while :
  do
    read -p "URL: " url
    
    echo "Clone default config from $url"
    read -p "Confirm? [y]" confirm
    if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ] && [ "$confirm" != "" ]; then
      read -p "Cancel config clone? [y]" confirm
      if [ "$confirm" == "y" ] || [ "$confirm" == "Y" ] || [ "$confirm" == "" ]; then
        break
      fi
    else
      repositoryPath="$nixosPath/repository"

      git clone --recursive --depth 1 "$url" "$repositoryPath" && \
        mv $repositoryPath/* $nixosPath/ && \
        mv $repositoryPath/.* $nixosPath/

      ([ -d "$repositoryPath" ] && rm -r "$repositoryPath" ) || true

      break
    fi
  done
fi

nixos-generate-config --root $installMount

while :
do
  echo
  read -p "Fetch public key? [y]" confirm
  if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ] && [ "$confirm" != "" ]; then
    break
  fi

  read -p "URL: " url
  read -p "Name: " name
  
  echo "Fetch public key '$name' from '$url'"
  read -p "Confirm? [y]" confirm
  if [ "$confirm" == "y" ] || [ "$confirm" == "Y" ] || [ "$confirm" == "" ]; then
    curl "$url" > "$srcPath/${name}.key.pub"
    ls -al "$srcPath/"
  fi
done

echo
echo "Transfer all resources to $srcPath/"
echo "Then edit $nixosPath/configuration.nix"
