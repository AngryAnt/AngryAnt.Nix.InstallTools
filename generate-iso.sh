#!/usr/bin/env nix-shell
#!nix-shell -i bash -p cdrtools

name=${PWD##*/}
isoName="${name}.iso"

if [ -z $name ]; then
  echo "Can not generate from file system root"
  exit
fi

cd ..
mkisofs -lJR -o $isoName $name
